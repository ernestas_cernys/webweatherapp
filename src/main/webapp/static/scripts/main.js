document.addEventListener("DOMContentLoaded", function () {
  const submitButton = document.getElementById("findForecast");
  const urlNominatim = "https://nominatim.openstreetmap.org/search";

  /**
   *
   * @param {*} url
   */
  function getData(url) {
    return fetch(url)
      .then((response) => {
        return response.json();
      })
      .catch((reject) => {
        window.alert(reject);
      });
  }

  /**
   *
   * @param {*} data
   */
  function fillLatLong(data) {
    const lat = document.getElementById("lat");
    const lon = document.getElementById("lon");

    if (data) {
      lat.value = data.lat;
      lon.value = data.lon;
    }

  }

  /**
   *
   */
  function generateForecast() {
    const city = document.getElementById("city");
    const finalUrl = urlNominatim + "?q=" + city.value + "&format=json";
    const form = document.getElementById("coordinates");

    if (city.value && city.value.trim() !== "") {
      if (getData(finalUrl)) {
        getData(finalUrl)
          .then((data) => {
            fillLatLong(data[0]);
          })
          .then(() => {
            form.submit();
          });
      }
    }
  }

  submitButton.addEventListener("click", function () {
    generateForecast();
  });
});
