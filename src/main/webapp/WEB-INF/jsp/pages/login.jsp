<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="Un projet simple pour apprendre connection aux API et httpServlet">
  <!-- CSS only -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <link rel="stylesheet"
        href="<c:url value="/static/styles/style.css" />">
  <title>Web Weather Application</title>
  <link rel="apple-touch-icon" sizes="180x180" href="<c:url value="/static/images/apple-touch-icon.png" />">
  <link rel="icon" type="image/png" sizes="32x32" href="<c:url value="/static/images/favicon-32x32.png" />">
  <link rel="icon" type="image/png" sizes="16x16" href="<c:url value="/static/images/favicon-16x16.png" />">
  <link rel="manifest" href="<c:url value="/static/images/site.webmanifest" />">
</head>

<body>

<!-- Navigation -->
<c:import url="/WEB-INF/jsp/_nav.jsp"/>

<div class="row mt-5">
  <div class="col-lg-8 col-md-10 mx-auto">

    <form action="<c:url value="/login"/>" method="post">

      <%-- csrf token generation --%>
<%--      <c:set var="csrfToken" value="${ TokenService.generateNewToken() }"/>--%>
<%--      <c:set target="${sessionScope}" property="${TokenService.CSRF_TOKEN_NAME}" value="${csrfToken}"/>--%>

      <div class="form-group row flex-grow-1 pb-3 mx-3">
        <label for="username" class=" px-3">Nom d'utilisateur</label>
        <input type="text" name="username" class=" form-control flex-grow-1"
               id="username" required>
      </div>

      <div class="form-group row flex-grow-1 pb-3 mx-3">
        <label for="password" class="px-3">Mot de passe</label>
        <input type="password" class="form-control flex-grow-1"
               id="password" name="password" required/>
      </div>

      <%-- csrf token--%>
<%--      <input type="text" name="<c:out value="${TokenService.CSRF_TOKEN_NAME}"/>"--%>
<%--             value="<c:out value="${csrfToken}"/>" hidden>--%>

      <div class="custom-control custom-checkbox d-flex align-items-center mb-2">
        <input type="checkbox" class="custom-control-input mt-2" id="remember_me" name="remember_me">
        <label class="custom-control-label" for="remember_me">Rester connecté</label>
      </div>

      <div class="row justify-content-between mx-3 mx-md-0">
        <button class="btn btn-primary mt-1" type="submit">
          Se Connecter
        </button>
<%--        <a class="btn btn-primary mt-1" type="button" href="<c:url value="/registration"/>">S'inscrire</a>--%>
      </div>
      <%--        <small>--%>
      <%--          <a class="form-text text-muted mx-3 mx-md-0" href="">Vous avez oublié votre mot de passe ?</a>--%>
      <%--        </small>--%>

    </form>


  </div>
</div>


<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>

<script type="text/javascript"
        src="<c:url value="/static/scripts/main.js" />"></script>
</body>
</html>
