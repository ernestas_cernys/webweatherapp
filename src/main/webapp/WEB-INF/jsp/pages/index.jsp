<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="Un projet simple pour apprendre connection aux API et httpServlet">
  <!-- CSS only -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <link rel="stylesheet"
        href="<c:url value="/static/styles/style.css" />">
  <title>Web Weather Application</title>
  <link rel="apple-touch-icon" sizes="180x180" href="<c:url value="/static/images/apple-touch-icon.png" />">
  <link rel="icon" type="image/png" sizes="32x32" href="<c:url value="/static/images/favicon-32x32.png" />">
  <link rel="icon" type="image/png" sizes="16x16" href="<c:url value="/static/images/favicon-16x16.png" />">
  <link rel="manifest" href="<c:url value="/static/images/site.webmanifest" />">
</head>

<body>

<!-- Navigation -->
<c:import url="/WEB-INF/jsp/_nav.jsp"/>

<section>
  <h1 class="text-center py-5">La Météo actuelle</h1>

  <form action="#" class="px-2" id="coordinates" method="get">

    <div class="form-inline">
      <label for="city" class="pr-2">Veuillez entrer la ville</label>
      <input type="text" class="form-control flex-grow-1" name="city"
             placeholder="Veuillez entrer l'adresse ou la ville" id="city">
    </div>

    <div class="form-inline mb-2">
      <input type="text" class="form-control flex-grow-1 mx-2" name="lon" id="lon" hidden>
      <input type="text" class="form-control flex-grow-1 mx-2" name="lat" id="lat" hidden>
    </div>

    <div>
      <label for="forecast">
        La météo à :
        <c:choose>
          <c:when test="${not empty requestScope.city}">
            <c:out value="${requestScope.city}"/>
          </c:when>
          <c:otherwise>
            <c:out value=""/>
          </c:otherwise>
        </c:choose>
      </label>
      <input type="text" class="form-control w-25 text-center" id="forecast" value="
        <c:choose>
          <c:when test="${not empty requestScope.weatherDescription}">
            <c:out value="${requestScope.weatherDescription}"/>
          </c:when>
          <c:otherwise>
            <c:out value=""/>
          </c:otherwise>
        </c:choose>
      " disabled>
    </div>

    <%-- comment --%>

    <button type="button" class="btn btn-primary my-3" id="findForecast">Genérer la méteo</button>
    <p><c:out value="${requestScope.test}"/></p>
    <p><c:out value="${requestScope.error}"/></p>

  </form>

</section>


<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>

<script type="text/javascript"
        src="<c:url value="/static/scripts/main.js" />"></script>
</body>
</html>
