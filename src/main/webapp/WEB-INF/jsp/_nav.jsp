<%@ page
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
  <div class="container mw-100">

    <a class="navbar-brand" href="<c:url value="/"/>">Web App</a>

    <button
      class=" navbar-toggler navbar-toggler-right"
      type="button"
      data-toggle="collapse"
      data-target="#navbarResponsive"
      aria-controls="navbarResponsive"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      Menu
      <i class="fas fa-bars"></i>
    </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">

      <ul class="navbar-nav ml-auto">

        <li class="nav-item">
          <a class="nav-link" href="<c:url value="/"/>">Acceuil</a>
        </li>


        <li id="login" class="nav-item" <c:if test="${not empty user}">hidden</c:if>>
          <c:if test="${not empty requestScope['javax.servlet.forward.query_string']}">
            <c:set var="queryString" scope="session" value="?${requestScope['javax.servlet.forward.query_string']}"/>
          </c:if>
          <a class="nav-link outline-secondary bg-dark"
             href="<c:url value="/login"/>">Login</a>
        </li>

        <li id="logout" class="nav-item outline-secondary bg-dark" <c:if test="${empty user}">hidden</c:if>>
          <a class="nav-link"
             href="<c:url value="/logout"/>">
            Logout | <c:out value="${user.userName}"/>
          </a>
        </li>

        <%--        sortie pour tester --%>
        <li class="nav-item">
          <a class="nav-link" href="#">Test : <c:out value="${sessionScope.test}"/></a>
        </li>


      </ul>
    </div>
  </div>
</nav>
