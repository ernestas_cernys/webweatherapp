package com.cernys.webWeatherApp;

import javax.annotation.Resource;
import javax.servlet.ServletException;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


import com.cernys.webWeatherApp.entities.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;



@WebServlet("/")
public class IndexServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;
  private final String OPEN_WEATHER_MAP_URL = "http://api.openweathermap.org/data/2.5/weather";
  private final String API_KEY = "4064e604fd2e0641e5806fd5d4c1f02d";
  private final String query = "SELECT * FROM sakila.actor;";
  private String test = "";
  final static Logger logger = LogManager.getLogger(IndexServlet.class);

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String lon = req.getParameter("lon");
    String lat = req.getParameter("lat");
    String city = req.getParameter(("city"));

    JSONParser parser = new JSONParser();
    String json = RequestFactory.getRequest(OPEN_WEATHER_MAP_URL + "?lat=" + lat + "&lon=" + lon + "&appid=" + API_KEY);

    if (json != null) {
      try {
        Object obj = parser.parse(json);
        JSONObject jsonObject = (JSONObject) obj;
        JSONArray weatherArray = (JSONArray) jsonObject.get("weather");
        JSONObject weatherObject = (JSONObject) weatherArray.get(0);
        String description = weatherObject.get("description").toString();

        req.setAttribute("weatherDescription", description);

      } catch (ParseException e) {
        req.setAttribute("error", "" + e);
        logger.error(e.getLocalizedMessage());

      }
    }

    req.setAttribute("test", getResult());
    req.setAttribute("city", city);
    // forward request and response to jsp file
    req.getRequestDispatcher("WEB-INF/jsp/pages/index.jsp").forward(req, resp);
  }

  @Resource (name = "jdbc/sakila")
  private DataSource ds;

  public String getResult (){

    //get database connection
    try (Connection connection = ds.getConnection()){

      PreparedStatement statement = connection.prepareStatement(query);
      ResultSet result = statement.executeQuery();
      while (result.next()) {
        test = test + result.getString("first_name") + "\n";
      }
    } catch (SQLException e) {
      test = e.toString();
      logger.error(e.getLocalizedMessage());
    }
    return test;


  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    JSONObject jsonObject = RequestFactory.getJSONFromPost(req);
    String userName = jsonObject.get("username").toString();

    User user = new User();
    user.setUserName(userName);
    req.getSession().setAttribute("user", user);
    req.getRequestDispatcher("/WEB-INF/jsp/pages/about.jsp").forward(req, resp);

  }
}
