package com.cernys.webWeatherApp;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * This example demonstrates the use of {@link HttpGet} request method.
 *
 * @author Ramesh Fadatare
 */
public class RequestFactory {

  public static String getRequest(String url) {
    String responseBody = null;
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

      // HTTP GET method
      HttpGet httpget = new HttpGet(url);

      // Create a custom response handler
      ResponseHandler<String> responseHandler = response -> {
        int status = response.getStatusLine().getStatusCode();
        if (status >= 200 && status < 300) {
          HttpEntity entity = response.getEntity();
          return entity != null ? EntityUtils.toString(entity) : null;
        } else {
          throw new ClientProtocolException("Unexpected response status: " + status);
        }
      };
      responseBody = httpclient.execute(httpget, responseHandler);
    } catch (Exception e) {

    }
    return responseBody;
  }

  public static String postRequest (String url, String data) throws Exception {
    String responseBody;
    try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

      HttpPost request = new HttpPost(url);
      StringEntity params = new StringEntity(data);
      request.addHeader("content-type", "application/json");
      request.setEntity(params);
      HttpResponse response = httpClient.execute(request);
      HttpEntity entity = response.getEntity();
      responseBody = entity != null ? EntityUtils.toString(entity) : null;
    }
    return responseBody;
  }

  public static JSONObject getJSONFromPost (HttpServletRequest request){
    StringBuffer jb = new StringBuffer();
    String line;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject = null;

    try (BufferedReader reader = request.getReader()) {
      while ((line = reader.readLine()) != null) {
        jb.append(line);
      }
      jsonObject =  (JSONObject) parser.parse(jb.toString());
    } catch (ParseException e) {
      request.getSession().setAttribute("test", "JSON parsing error");
    } catch (IOException e) {
      request.getSession().setAttribute("test", "Buffered reader error");
    }

    return  jsonObject;
  }

}
