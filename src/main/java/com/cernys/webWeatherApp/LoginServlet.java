package com.cernys.webWeatherApp;

import com.cernys.webWeatherApp.entities.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    req.getRequestDispatcher("WEB-INF/jsp/pages/login.jsp").forward(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    JSONParser parser = new JSONParser();
    String userName = req.getParameter("username");
    String password = req.getParameter("password");

    JSONObject jsonObject = new JSONObject();
    jsonObject.put("username", userName);
    jsonObject.put("password", password);

    try {
      String json = RequestFactory.postRequest("http://localhost:8080/DevBlog/ExternalAuthentication", jsonObject.toJSONString());
      if (json != null) {
        try {
          JSONObject obj = (JSONObject) parser.parse(json);
          String resultUserName = (String) obj.get("username");
          User user = new User();
          user.setUserName(resultUserName);
          req.getSession().setAttribute("user", user);

        } catch (ParseException e) {
          req.getSession().setAttribute("test", "error fetching data from api DevBlog");
        }
      }
    } catch (Exception e) {
      req.getSession().setAttribute("test", "error fetching data from api DevBlog");
    }
    resp.sendRedirect(req.getContextPath());
  }
}
